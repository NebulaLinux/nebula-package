Format: 1.0
Source: nebula-package
Binary: nebula-package
Architecture: all
Version: 0.1
Maintainer: Ernesto Crespo <ecrespo@gmail.com>
Homepage: http://nebula
Standards-Version: 3.9.2
Build-Depends: debhelper (>= 7)
Package-List:
 nebula-package deb misc optional arch=all
Checksums-Sha1:
 b04094e1012325d02f838f7e85c9eece5267ac81 1866 nebula-package_0.1.tar.gz
Checksums-Sha256:
 d81d62e3db9fbb1f14e261d6bf32434da354de60533b446f5ab383ee71a07389 1866 nebula-package_0.1.tar.gz
Files:
 269b211cf621276ccc4f58fa98e5c986 1866 nebula-package_0.1.tar.gz
